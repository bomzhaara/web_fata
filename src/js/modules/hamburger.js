(function() {
	const hamburgers = document.querySelectorAll('.btn-hamburger');
	if (hamburgers[0]) {
		hamburgers.forEach((humburger) => {
			humburger.addEventListener('click', () => {
				if (humburger.classList.contains('active')) {
					humburger.classList.remove('active');
				} else {
					humburger.classList.add('active');
				}
			});
		});
	}
})();
